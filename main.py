from PIL import Image
import io


def encodemessage(message):
    binary = []
    for character in message:
        for bit in format(ord(character), "b").zfill(8):
            binary.append(bit)
    return binary

def modifypixel(pixel, bit, color):
    r, g, b = pixel
    if color == "r":
        r = format(r, "b")[:-1] + str(bit)
        return int(r, 2), g, b
    elif color == "g":
        g = format(g, "b")[:-1] + str(bit)
        return r, int(g, 2), b
    elif color == "b":
        b = format(b, "b")[:-1] + str(bit)
        return r, g, int(b, 2)

def readpixel(pixel, color):
    r, g, b = pixel
    if color == "r":
        return format(r, "b")[-1:]
    elif color == "g":
        return format(g, "b")[-1:]
    elif color == "b":
        return format(b, "b")[-1:]


def checkrgb(pattern):
    '''
    Takes a rgb pattern in form of a string and checks 
    if that string only contains the characters r,b, and g.
    If it only contains the chars r,g,b it returns true, otherwise it returns false
    '''
    rgb = list('rgb')
    for c in pattern.lower():
        if c not in rgb: 
            raise Exception("The pattern given contains other letters than r, g, and b")
    return True

def checkroom(binary_message, height, width):
    if len(binary_message) < height * width:
        return True
    else:
        raise Exception("The encrypted text can't be fitted into the chosen picture'")


def merge(path, message, pattern, destination):
    '''
    Takes  for the picture for encryption, the message, and the pattern used for
    the encryption. once the picture is encrypted it saves the picture to the given path
    '''
    binary_message = encodemessage(message)
    image = Image.open(path)
    width, height = image.size
    pixels = image.load()

    if checkrgb(pattern) and checkroom(binary_message, width, height):
        counter = 0
        pattern = list(pattern)
        for x in xrange(image.width):
            for y in xrange(image.height):
                if binary_message:
                    pixels[x, y] = modifypixel(pixels[x, y], 
                                            binary_message[0], 
                                            pattern[counter%len(pattern)])
                    binary_message.pop(0)
                    counter += 1
                else:
                    image.save(destination)
                    return


def readpicture(path, pattern):
    '''Takes a path for a picture and a pattern used for decrypting the message'''
    counter = 0
    pattern = list(pattern)
    temp_string = ""
    decrypted_string = ""

    image = Image.open(path)
    width, height = image.size
    pixels = image.load()

    for x in xrange(image.width):
        for y in xrange(image.height):
            temp_string += readpixel(pixels[x, y], pattern[counter%len(pattern)])
            if len(temp_string) == 8:
                decrypted_string += chr(int(temp_string, 2))
                temp_string = ""
            counter += 1
    return decrypted_string


message = "this is yet an other message for the test of that test"
merge("ubuntu.png", message, 'brbggg' "testpicture.png")
print readpicture("testimage.png", 'brbggg')






